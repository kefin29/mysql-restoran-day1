CREATE DATABASE restoran;
USE restoran;

CREATE TABLE `menu` (
  `menuid` int NOT NULL,
  `menuname` varchar(100) NOT NULL,
  `menudetail` varchar(100) NOT NULL,
  `menuprice` varchar(100) NOT NULL,
  `eventid` int NOT NULL,
  primary key(menuid),
  foreign key(eventid) references event(eventid)
);

CREATE TABLE `ordered` (
  `orderid` int NOT NULL,
  `ordercustomer` varchar(100) NOT NULL,
  `orderdetailid` varchar(100) NOT NULL,
  `ordertotalprice` int NOT NULL,
  primary key(orderid),
  foreign key(orderdetailid) references orderdetail(orderdetailid)
);

CREATE TABLE `orderdetail` (
  `orderdetailid` int NOT NULL,
  `menuid` varchar(100) NOT NULL,
  `orderquantity` int NOT NULL,
  `ordersubtotal` int NOT NULL,
  primary key(orderdetailid)
);

CREATE TABLE `event` (
  `eventid` int NOT NULL,
  `eventname` varchar(100) NOT NULL,
  `startdate` datetime NOT NULL,
  `expireddate` datetime NOT NULL,
  primary key(eventid)
);